package com.sticreations.rockpaperscissors.service

import com.sticreations.rockpaperscissors.model.Player
import com.sticreations.rockpaperscissors.model.PlayerChoice
import com.sticreations.rockpaperscissors.model.Scoreboard
import mu.KotlinLogging
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

val logger = KotlinLogging.logger {}

interface RockPaperScissors {
    fun play(p1: Player, p2: Player, rounds: Int): Scoreboard
}

class RockPaperScissorsImpl : RockPaperScissors {

    override fun play(p1: Player, p2: Player, rounds: Int): Scoreboard =
        Flux.fromIterable(
            generateSequence { Pair(p1, p2) }
                .take(rounds).toList()
        )
            .flatMap { Mono.just(playRound(it)) }
            .reduce { result, nextResult ->
                Scoreboard(
                    result.draw + nextResult.draw,
                    result.playerOneWins + nextResult.playerOneWins,
                    result.playerTwoWins + nextResult.playerTwoWins
                )
            }
            .doOnNext {
                logger.info { winningText(it) }
            }
            .block()!!

    private val winningText = { it: Scoreboard ->
        "The Reults are in!\n" +
            "Player 1 has won : ${it.playerOneWins} times \n" +
            "Player 2 has won ${it.playerTwoWins} times \n" +
            "there were ${it.draw} draws. \n" +
            getWinner(it)
    }

    private val getWinner = { scoreboard: Scoreboard ->
        when (scoreboard.playerOneWins > scoreboard.playerTwoWins) {
            true -> "Congratulations Player One you've won the match!"
            false -> "Congratulations Player Two you've won the match!"
        }
    }

    private fun playRound(players: Pair<Player, Player>): Scoreboard {
        val playerOneHandsign = players.first.getChoice()
        val playerTwoHandsign = players.second.getChoice()
        if (playerOneHandsign == playerTwoHandsign)
            return Scoreboard(draw = 1)
        else if (playerOneHandsign == PlayerChoice.ROCK && playerTwoHandsign == PlayerChoice.SCISSORS) return Scoreboard(
            playerOneWins = 1
        )
        else if (playerOneHandsign == PlayerChoice.PAPER && playerTwoHandsign == PlayerChoice.ROCK) return Scoreboard(
            playerOneWins = 1
        )
        else if (playerOneHandsign == PlayerChoice.SCISSORS && playerTwoHandsign == PlayerChoice.PAPER) return Scoreboard(
            playerOneWins = 1
        )
        return Scoreboard(playerTwoWins = 1)
    }
}
