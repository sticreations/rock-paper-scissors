package com.sticreations.rockpaperscissors

import com.sticreations.rockpaperscissors.model.Player
import com.sticreations.rockpaperscissors.model.PlayerChoice
import com.sticreations.rockpaperscissors.service.RockPaperScissors
import com.sticreations.rockpaperscissors.service.RockPaperScissorsImpl

var rockPaperScissors: RockPaperScissors = RockPaperScissorsImpl()

fun main(args: Array<String>) {
    rockPaperScissors.play(Player.create(), Player.create(PlayerChoice.ROCK), 100)
}
