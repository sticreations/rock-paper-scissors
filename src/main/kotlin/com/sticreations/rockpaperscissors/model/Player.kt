package com.sticreations.rockpaperscissors.model

import kotlin.math.floor

data class Player(
    val playerChoice: () -> PlayerChoice,
) {
    companion object {
        fun create(playerchoice: PlayerChoice): Player {
            return Player { playerchoice }
        }

        fun create(): Player {
            return Player { PlayerChoice.from(floor(Math.random() * 3).toInt()) }
        }
    }

    fun getChoice(): PlayerChoice {
        return this.playerChoice.invoke()
    }
}

enum class PlayerChoice {
    ROCK,
    PAPER,
    SCISSORS;

    companion object {
        fun from(findValue: Int): PlayerChoice = values().get(findValue)
    }
}
