package com.sticreations.rockpaperscissors.model

data class Scoreboard(
    val draw: Int = 0,
    val playerOneWins: Int = 0,
    val playerTwoWins: Int = 0,
)
