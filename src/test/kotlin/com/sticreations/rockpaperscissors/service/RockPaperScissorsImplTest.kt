package com.sticreations.rockpaperscissors.service

import com.sticreations.rockpaperscissors.model.Player
import com.sticreations.rockpaperscissors.model.PlayerChoice
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RockPaperScissorsImplTest {

    var rockPaperScissors = RockPaperScissorsImpl()

    @Test
    fun `given Player1 with Paper and Player2 with Rock, when playing one Round then validate Player 1 as Winner`() {
        val res = rockPaperScissors.play(
            Player.create(PlayerChoice.PAPER),
            Player.create(PlayerChoice.ROCK),
            1
        )
        assertEquals(1, res.playerOneWins)
    }

    @Test
    fun `given Player1 with Scissors and Player2 with Paper, when playing one Round  then validate Player 1 as Winner`() {
        val res = rockPaperScissors.play(
            Player.create(PlayerChoice.SCISSORS),
            Player.create(PlayerChoice.PAPER),
            1
        )
        assertEquals(1, res.playerOneWins)
    }

    @Test
    fun `given Player1 with Rock and Player2 with Scissors, when playing one Round  then validate Player 1 as Winner`() {
        val res = rockPaperScissors.play(
            Player.create(PlayerChoice.ROCK),
            Player.create(PlayerChoice.SCISSORS),
            1
        )
        assertEquals(1, res.playerOneWins)
    }

    @Test
    fun `given Player 1 with Scissors and Player 2 with Rock, when playing one Round then validate Player 2 as Winner`() {
        val res = rockPaperScissors.play(
            Player.create(PlayerChoice.SCISSORS),
            Player.create(PlayerChoice.ROCK),
            1
        )
        assertEquals(1, res.playerTwoWins)
    }

    @Test
    fun `given Player1 with Rock and Player2 with Rock, when playing one Round then counter of Draws is increased`() {
        val res = rockPaperScissors.play(
            Player.create(PlayerChoice.ROCK),
            Player.create(PlayerChoice.ROCK),
            1
        )
        assertEquals(1, res.draw)
    }

    @Test
    fun `given P1 Rock, P2 Scissors, when playing 100 Rounds should return 100 Wins for P1`() {
        val res = rockPaperScissors.play(
            Player.create(PlayerChoice.ROCK),
            Player.create(PlayerChoice.SCISSORS),
            100
        )
        assertEquals(100, res.playerOneWins)
    }

    @Test
    fun `given P1 random, P2 Rock, when playing 1000000 Rounds, should WIN + DRAW + LOSE accumulate to 100`() {
        val res = rockPaperScissors.play(
            Player.create(),
            Player.create(PlayerChoice.ROCK),
            1000000
        )
        assertEquals(1000000, res.draw + res.playerOneWins + res.playerTwoWins)
    }
}
