import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.10"
    kotlin("kapt") version "1.4.10"
    id("jacoco")
    id("org.jlleitschuh.gradle.ktlint") version "9.4.1"
}
group = "com.sticreations.rockpaperscissors"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://dl.bintray.com/arrow-kt/arrow-kt/")

    dependencies {
        /**
         * Reactor & Logging
         */
        implementation("io.projectreactor:reactor-core:3.3.10.RELEASE")
        implementation("io.github.microutils:kotlin-logging:1.12.0")
        implementation("org.slf4j:slf4j-simple:1.7.25")
        /**
         * Test Dependencies
         **/
        testImplementation(platform("org.junit:junit-bom:5.7.0"))
        testImplementation("org.junit.jupiter:junit-jupiter")
    }

    tasks.withType<KotlinCompile>() {
        kotlinOptions.jvmTarget = "11"
    }

    tasks.test {
        useJUnitPlatform()
        finalizedBy("jacocoTestReport")
        doLast {
            println("View code coverage at:")
            println("file://$buildDir/reports/jacoco/test/html/index.html")
        }
    }
}
